
exports.seed = knex => knex.schema.createTable('posts', function (table) {
    table.increments();
    table.string('title', 500).notNullable();
    table.text('content', 'longtext').notNullable();
    table.timestamps(false, true);
});
