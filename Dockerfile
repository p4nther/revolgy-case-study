FROM node:latest

RUN mkdir /opt/app

COPY . /opt/app

WORKDIR /opt/app

RUN yarn --prod

CMD [ "npm", "run", "start" ]