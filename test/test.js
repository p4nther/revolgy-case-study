const assert = require('assert')

describe('Functions', function () {
    describe('calculateReadTime()', function () {
        const calculateReadTime = require('../src/calculateReadTime')

        it('should return 0 when the string length is 0', function () {
            assert.equal(calculateReadTime(""), 0)
        })

        it('should return 4 when the string length is 789', function () {
            let str = "sdfsdfsdf ";
            for (let i = 0; i < 789; i += 1) {
                str = str + "a";
            }

            assert.equal(calculateReadTime(str), 4)
        })

        it('should return 5 when the string length is 841', function () {
            let str = "sdfsdfsdf ";
            for (let i = 0; i < 841; i += 1) {
                str = str + "a";
            }

            assert.equal(calculateReadTime(str), 5)
        })
    })
})
