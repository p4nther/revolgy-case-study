# Revolgy - case study

The goal of this task is to set up sample production grade infrastructure based on GitOps.

## Description

- ECS cluster with one service and two tasks
- capacity provided by EC2 autoscalling group
- MySQL 5.7 used as database
- build phase done in Bitbucket Pipelines
- Docker images from Bitbucket Pipelines pushed to Amazon ECR
- CodePipeline triggered via CloudWatch event rule

## Application

- simple Node.js app written using Express.js
- authentication done via Amazon Cognito and JWT tokens on API Gateway
- database migrations done via Knex and are triggered after succesful build from Bitbucket Pipelines
- local development done via Docker Compose

## Infrastructure
- VPC with two public subnets
- ECS cluster with Elastic LoadBalancer
- RDS MySQL database
- API Gateway
- Cognito user pool
- CloudWatch logging and monitoring
- CodePipeline for deployment
- no paid services used, everything under AWS's free tier