
exports.up = knex => knex('posts').insert([
    {'title': 'Hello', 'content': 'world!'},
    {
        'title': 'Revolgy',
        'content': '...a bunch of cloud-native enthusiasts. Currently, there are about 60 of us. Leading Czech cloud-services company, on the market since 1996. Certified and trusted partner of Google and AWS, providing digital transformation services through cloud infrastructure and online collaboration tools. Helping our 2000+ B2B (Baltics 2 Balkan) clients grow their business in the cloud.'
    },
]);

exports.down = knex => knex('posts').truncate();
