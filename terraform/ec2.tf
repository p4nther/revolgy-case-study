resource "aws_security_group" "allow_http" {
  name        = "revolgy_allow_http"
  description = "Allow HTTP/s trafic"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = "80"
    to_port     = "80"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  ingress {
    protocol  = "tcp"
    from_port = 32768
    to_port   = 61000

    security_groups = [
      aws_security_group.loadbalancer.id,
    ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_iam_policy_document" "ecs_instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_instance_role" {
  name               = "ecs-instance-role-revolgy"
  assume_role_policy = data.aws_iam_policy_document.ecs_instance_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_policy" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_instance_profile" {
  name = "ecsInstanceRole-revolgy"
  path = "/"
  role = aws_iam_role.ecs_instance_role.name
}

resource "aws_key_pair" "ondrejsatera" {
  key_name   = "ondrejsatera-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCr13YjkMu+754Vr5+kvd+gxxfYuW9W71WHkVRgr7w2qgkz2PAfwjpYn48NvL50uQsYXhvC8UAGCFn/kcH+UIjwzZnMl/miZ00ppTpiwKbbnbyTMkjQ4QYNWMrewpb9KM8evwlVd9Uh54HO1MwLpEIDHE5dMW5TSo5x1DQXTaooC6wppXWEgZZBPZSVJhXlqrhGxZVKuIwER/kU+YOEOcRod5sDdoo6VvtbTGwMiCbGtX67YwT+bo30TD7WAgutAaA74cXXAoqVwi7Ib1jqW6yfYS+s7oIrn3Fka88SxtVrMqJObixJGft0KCYkL96ooONVmRAJnrswLpH2bYZ9ewBB ondrejsatera@ondrej-macbook-pro-2.home"
}

resource "aws_launch_configuration" "main" {
  name_prefix = format("ecs-%s-", var.ecs_cluster_name)

  instance_type = var.ec2_instance_type
  image_id      = var.ec2_instance_ami_id

  associate_public_ip_address = true
  security_groups             = [aws_security_group.allow_http.id]
  iam_instance_profile        = aws_iam_instance_profile.ecs_instance_profile.name

  key_name = aws_key_pair.ondrejsatera.key_name

  root_block_device {
    volume_type = "standard"
  }

  ebs_block_device {
    device_name = "/dev/xvdcz"
    volume_type = "standard"
    encrypted   = true
    volume_size = 8
  }

  user_data = <<EOF
#!/bin/bash
# The cluster this agent should check into.
echo 'ECS_CLUSTER=${var.ecs_cluster_name}' >> /etc/ecs/ecs.config
# Disable privileged containers.
echo 'ECS_DISABLE_PRIVILEGED=true' >> /etc/ecs/ecs.config
EOF


  lifecycle {
    create_before_destroy = true
  }

  enable_monitoring = true
}

resource "aws_autoscaling_group" "main" {
  max_size            = 1
  min_size            = 0
  desired_capacity    = 1
  name                = "revolgy-case-study"
  vpc_zone_identifier = [aws_subnet.public.0.id]
  health_check_type   = "EC2"

  launch_configuration = aws_launch_configuration.main.id

  lifecycle {
    create_before_destroy = true
  }
}