resource "aws_ecr_repository" "main" {
  name = "revolgy_app"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_iam_service_linked_role" "ecs" {
  aws_service_name = "ecs.amazonaws.com"
}

resource "aws_ecs_capacity_provider" "ec2" {
  name = "revolgy-ec2"

  auto_scaling_group_provider {
    auto_scaling_group_arn = aws_autoscaling_group.main.arn

    managed_scaling {
      status = "DISABLED"
    }
  }

  depends_on = [aws_iam_service_linked_role.ecs]
}

data "aws_iam_policy_document" "ecs_service_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_service" {
  name = "tf_example_ecs_role"

  assume_role_policy = data.aws_iam_policy_document.ecs_service_assume_role_policy.json
}

data "aws_iam_policy_document" "ecs_service_elb_policy" {
  statement {
    actions = [
      "ec2:Describe*",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:Describe*",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "elasticloadbalancing:RegisterTargets"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role_policy" "ecs_service" {
  name = "tf_example_ecs_policy"
  role = aws_iam_role.ecs_service.name

  policy = data.aws_iam_policy_document.ecs_service_elb_policy.json
}

resource "aws_ecs_cluster" "main" {
  name = var.ecs_cluster_name

  capacity_providers = [aws_ecs_capacity_provider.ec2.name]

  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ec2.name
  }
}

data "template_file" "revolgy_app_template" {
  template = file("task-definitions/service.json.tpl")

  vars = {
    ecr_host             = aws_ecr_repository.main.repository_url
    app_name             = var.app_name
    app_port             = var.app_port
    app_host             = var.app_host
    db_host              = aws_db_instance.default.address
    db_port              = aws_db_instance.default.port
    db_name              = var.rds_name
    db_user              = var.rds_username
    db_pass              = var.rds_password
    log_group_region     = var.aws_region
    log_group_name       = aws_cloudwatch_log_group.app.name
    cognito_user_pool_id = aws_cognito_user_pool.main_pool.id
    cognito_client_id    = aws_cognito_user_pool_client.client.id

    # command has to be an array and it cannot be fetched from variables directly
    job = "\"command\": [\n  \"npm\", \"run\", \"${var.app_command}\"\n]"
  }
}

data "template_file" "revolgy_migration_template" {
  template = file("task-definitions/service.json.tpl")

  vars = {
    ecr_host             = aws_ecr_repository.main.repository_url
    app_name             = var.app_name
    app_port             = var.app_port
    app_host             = var.app_host
    db_host              = aws_db_instance.default.address
    db_port              = aws_db_instance.default.port
    db_name              = var.rds_name
    db_user              = var.rds_username
    db_pass              = var.rds_password
    log_group_region     = var.aws_region
    log_group_name       = aws_cloudwatch_log_group.app_migrations.name
    cognito_user_pool_id = aws_cognito_user_pool.main_pool.id
    cognito_client_id    = aws_cognito_user_pool_client.client.id

    # command has to be an array and it cannot be fetched from variables directly
    job = "\"command\": [\n  \"npm\", \"run\", \"${var.app_command_migration}\"\n]"
  }
}

resource "aws_ecs_task_definition" "service" {
  family                = "service"
  container_definitions = data.template_file.revolgy_app_template.rendered
}

resource "aws_ecs_task_definition" "service_migration" {
  family                = "service_migration"
  container_definitions = data.template_file.revolgy_migration_template.rendered
}

resource "aws_ecs_service" "main" {
  name                 = "revolgy-service"
  cluster              = aws_ecs_cluster.main.id
  task_definition      = aws_ecs_task_definition.service.arn
  desired_count        = 1
  force_new_deployment = true
  iam_role             = aws_iam_role.ecs_service.name

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ec2.name
    weight            = 1
    base              = 1
  }

  ordered_placement_strategy {
    type = "random"
  }

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.main.id
    container_name   = var.app_name
    container_port   = var.app_port
  }

  depends_on = [aws_alb_listener.main, aws_iam_role_policy.ecs_service]
}