[
  {
    "name": "${app_name}",
    "image": "${ecr_host}:latest",
    "cpu": 100,
    "memory": 128,
    "essential": true,
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": 0
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${log_group_name}",
        "awslogs-region": "${log_group_region}"
      }
    },
    "environment": [
      {
        "name": "DB_HOST",
        "value": "${db_host}"
      },
      {
        "name": "DB_PORT",
        "value": "${db_port}"
      },
      {
        "name": "DB_NAME",
        "value": "${db_name}"
      },
      {
        "name": "DB_USER",
        "value": "${db_user}"
      },
      {
        "name": "DB_PASS",
        "value": "${db_pass}"
      },
      {
        "name": "APP_HOST",
        "value": "${app_host}"
      },
      {
        "name": "APP_PORT",
        "value": "${app_port}"
      },
      {
        "name": "NODE_ENV",
        "value": "production"
      },
      {
        "name": "COGNITO_USER_POOL_ID",
        "value": "${cognito_user_pool_id}"
      },
      {
        "name": "COGNITO_CLIENT_ID",
        "value": "${cognito_client_id}"
      }
    ],
    ${job}
  }
]
