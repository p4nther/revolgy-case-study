resource "aws_security_group" "loadbalancer" {
  name   = "revolgy-alb-security-group"
  vpc_id = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_alb" "main" {
  name                       = "revolgy-lb"
  subnets                    = aws_subnet.public.*.id
  security_groups            = [aws_security_group.loadbalancer.id]
  internal                   = false
  enable_deletion_protection = false
}

resource "aws_alb_target_group" "main" {
  name                 = "revolgy-target-group"
  port                 = var.app_host_port
  protocol             = "HTTP"
  vpc_id               = aws_vpc.main.id
  target_type          = "instance"
  deregistration_delay = 180

  health_check {
    healthy_threshold   = "2"
    interval            = "45"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "30"
    path                = "/"
    unhealthy_threshold = "6"
  }
}

resource "aws_alb_listener" "main" {
  load_balancer_arn = aws_alb.main.id
  port              = var.app_host_port
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.main.id
    type             = "forward"
  }

  depends_on = [aws_alb.main, aws_alb_target_group.main]
}