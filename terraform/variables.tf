variable "aws_region" {
  default = "eu-west-1"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "rds_username" {
  description = "Master user name"
  default     = "revolgy"
}

variable "rds_name" {
  description = "Main database name"
  default     = "revolgy"
}

variable "rds_storage" {
  description = "Main database storage size (GBs)"
  default     = 20
}

variable "rds_storage_type" {
  description = "Main database storage type"
  default     = "gp2"
}

variable "rds_engine" {
  description = "Main database engine (MySQL, PostgreSQL, Aurora)"
  default     = "mysql"
}

variable "rds_engine_version" {
  description = "Main database engine version"
  default     = "5.7"
}

variable "rds_instance_class" {
  description = "Main database instance class"
  default     = "db.t2.micro"
}

variable "rds_parameter_group" {
  description = "Main database parameter group"
  default     = "default.mysql5.7"
}

variable "rds_publicly_accessible" {
  description = "Whether the RDS is accessible from the internet"
  default     = false
}

variable "ecs_cluster_name" {
  description = "Name for the main ECS cluster"
  default     = "revolgy-cluster"
}

variable "ec2_instance_type" {
  description = "EC2 instance class for container instances"
  default     = "t2.micro"
}

variable "ec2_instance_ami_id" {
  description = "EC2 instance AMI ID for container instances"
  default     = "ami-0bb01c7d2705a4800"
}

variable "app_port" {
  description = "On which port should be the Node.js running"
  default     = 3000
}

variable "app_host_port" {
  description = "On which port should be the Node.js accessible"
  default     = 80
}

variable "app_name" {
  description = "How the Node.js container should be named"
  default     = "node_app"
}

variable "app_host" {
  description = "App address inside container"
  default     = "0.0.0.0"
}

variable "app_command" {
  description = "Main command for Node.js app"
  default     = "start"
}

variable "app_command_migration" {
  description = "Command for Node.js app to run database migrations"
  default     = "init"
}
