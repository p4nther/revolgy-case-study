resource "aws_apigatewayv2_api" "main" {
  name          = "revolgy-http-api"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_integration" "alb_get" {
  api_id               = aws_apigatewayv2_api.main.id
  integration_type     = "HTTP_PROXY"
  integration_method   = "GET"
  integration_uri      = "http://${aws_alb.main.dns_name}"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_integration" "alb_get_post" {
  api_id               = aws_apigatewayv2_api.main.id
  integration_type     = "HTTP_PROXY"
  integration_method   = "GET"
  integration_uri      = "http://${aws_alb.main.dns_name}/posts/{postID}"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_integration" "alb_post" {
  api_id               = aws_apigatewayv2_api.main.id
  integration_type     = "HTTP_PROXY"
  integration_method   = "POST"
  integration_uri      = "http://${aws_alb.main.dns_name}/posts"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_integration" "alb_get_posts" {
  api_id               = aws_apigatewayv2_api.main.id
  integration_type     = "HTTP_PROXY"
  integration_method   = "GET"
  integration_uri      = "http://${aws_alb.main.dns_name}/posts"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_integration" "alb_post_register" {
  api_id               = aws_apigatewayv2_api.main.id
  integration_type     = "HTTP_PROXY"
  integration_method   = "POST"
  integration_uri      = "http://${aws_alb.main.dns_name}/auth/register"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_integration" "alb_post_login" {
  api_id               = aws_apigatewayv2_api.main.id
  integration_type     = "HTTP_PROXY"
  integration_method   = "POST"
  integration_uri      = "http://${aws_alb.main.dns_name}/auth/login"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_authorizer" "main" {
  api_id           = aws_apigatewayv2_api.main.id
  authorizer_type  = "JWT"
  identity_sources = ["$request.header.Authorization"]
  name             = "cognito-authorizer"

  jwt_configuration {
    audience = [aws_cognito_user_pool_client.client.id]
    issuer   = "https://${aws_cognito_user_pool.main_pool.endpoint}"
  }
}

resource "aws_apigatewayv2_route" "default" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "$default"
  target    = "integrations/${aws_apigatewayv2_integration.alb_get.id}"

  depends_on = [aws_apigatewayv2_integration.alb_get]
}

resource "aws_apigatewayv2_route" "secured_route_get_list" {
  api_id             = aws_apigatewayv2_api.main.id
  route_key          = "GET /posts"
  authorization_type = "JWT"
  authorizer_id      = aws_apigatewayv2_authorizer.main.id
  target             = "integrations/${aws_apigatewayv2_integration.alb_get_posts.id}"

  depends_on = [aws_apigatewayv2_integration.alb_get_posts]
}

resource "aws_apigatewayv2_route" "secured_route_get_detail" {
  api_id             = aws_apigatewayv2_api.main.id
  route_key          = "GET /posts/{postID}"
  authorization_type = "JWT"
  authorizer_id      = aws_apigatewayv2_authorizer.main.id
  target             = "integrations/${aws_apigatewayv2_integration.alb_get_post.id}"

  depends_on = [aws_apigatewayv2_integration.alb_get_post]
}

resource "aws_apigatewayv2_route" "secured_route_post" {
  api_id             = aws_apigatewayv2_api.main.id
  route_key          = "POST /posts"
  authorization_type = "JWT"
  authorizer_id      = aws_apigatewayv2_authorizer.main.id
  target             = "integrations/${aws_apigatewayv2_integration.alb_post.id}"

  depends_on = [aws_apigatewayv2_integration.alb_post]
}

resource "aws_apigatewayv2_route" "route_register" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "POST /auth/register"
  target    = "integrations/${aws_apigatewayv2_integration.alb_post_register.id}"

  depends_on = [aws_apigatewayv2_integration.alb_post_register]
}

resource "aws_apigatewayv2_route" "route_login" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "POST /auth/login"
  target    = "integrations/${aws_apigatewayv2_integration.alb_post_login.id}"

  depends_on = [aws_apigatewayv2_integration.alb_post_login]
}

resource "aws_apigatewayv2_stage" "main" {
  api_id      = aws_apigatewayv2_api.main.id
  name        = "$default"
  auto_deploy = true
}