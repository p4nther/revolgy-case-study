resource "aws_cognito_user_pool" "main_pool" {
  name = "revolgy_pool"

  mfa_configuration = "OFF"

  password_policy {
    minimum_length                   = 8
    require_lowercase                = true
    require_numbers                  = true
    require_symbols                  = true
    temporary_password_validity_days = 7
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name = "revolgy_client"

  user_pool_id = aws_cognito_user_pool.main_pool.id
}