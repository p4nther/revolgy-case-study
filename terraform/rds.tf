resource "aws_security_group" "allow_rds_local" {
  name        = "revolgy_allow_rds_local"
  description = "Allow local trafic to MySQL instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = "3306"
    to_port     = "3306"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_subnet_group" "sg" {
  name       = "main-subnet-group"
  subnet_ids = aws_subnet.public.*.id
}

resource "aws_db_instance" "default" {
  allocated_storage      = var.rds_storage
  storage_type           = var.rds_storage_type
  engine                 = var.rds_engine
  engine_version         = var.rds_engine_version
  instance_class         = var.rds_instance_class
  name                   = var.rds_name
  username               = var.rds_username
  password               = var.rds_password
  parameter_group_name   = var.rds_parameter_group
  publicly_accessible    = var.rds_publicly_accessible
  vpc_security_group_ids = [aws_security_group.allow_rds_local.id]
  db_subnet_group_name   = aws_db_subnet_group.sg.id
}