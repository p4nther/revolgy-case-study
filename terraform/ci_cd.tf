resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = "revolgy-case-study-072020-pipeline"
  acl    = "private"

  versioning {
    enabled = true
  }
}

data "aws_iam_policy_document" "pipeline_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "pipeline_s3_policy" {
  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketVersioning",
      "s3:PutObject"
    ]

    resources = [
      aws_s3_bucket.codepipeline_bucket.arn,
      "${aws_s3_bucket.codepipeline_bucket.arn}/*"
    ]
  }

  statement {
    actions = [
      "codebuild:BatchGetBuilds",
      "codebuild:StartBuild"
    ]

    resources = ["*"]
  }

  statement {
    actions = [
      "ecr:DescribeImages"
    ]

    resources = ["*"]
  }

  statement {
    actions = [
      "ecs:*",
    ]

    resources = ["*"]
  }

  statement {
    actions = [
      "iam:PassRole",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "codepipeline_policy"
  role = aws_iam_role.codepipeline_role.id

  policy = data.aws_iam_policy_document.pipeline_s3_policy.json
}

resource "aws_iam_role" "codepipeline_role" {
  name = "pipeline-role"

  assume_role_policy = data.aws_iam_policy_document.pipeline_assume_role_policy.json
}

resource "aws_codepipeline" "codepipeline" {
  name     = "revolgy-case-study"
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline_bucket.bucket
    type     = "S3"
  }

  stage {
    name = "ImageSource"

    action {
      category = "Source"
      name     = "Source"
      owner    = "AWS"
      provider = "ECR"
      version  = "1"

      configuration = {
        RepositoryName = aws_ecr_repository.main.name
        ImageTag       = "latest"
      }

      output_artifacts = ["SourceArtifact"]
    }

    action {
      category = "Source"
      name     = "ImageDefinitionsSource"
      owner    = "AWS"
      provider = "S3"
      version  = "1"

      configuration = {
        S3Bucket    = aws_s3_bucket.codepipeline_bucket.bucket
        S3ObjectKey = "imagedefinitions.zip"
      }

      output_artifacts = ["imagedefinitions"]
    }
  }

  stage {
    name = "Deploy"

    action {
      category = "Deploy"
      name     = "Deploy"
      owner    = "AWS"
      provider = "ECS"
      version  = "1"

      configuration = {
        ClusterName = aws_ecs_cluster.main.name
        ServiceName = aws_ecs_service.main.name
      }

      input_artifacts = ["imagedefinitions"]
    }
  }
}

data "aws_iam_policy_document" "cw_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "cw_ecs_pipeline" {
  assume_role_policy = data.aws_iam_policy_document.cw_assume_role.json
}

data "aws_iam_policy_document" "cw_pipeline_start" {
  statement {
    actions = [
      "codepipeline:StartPipelineExecution",
    ]

    resources = [aws_codepipeline.codepipeline.arn]
  }
}

resource "aws_iam_role_policy" "cw_pipeline_start" {
  name   = "cw_pipeline_start"
  policy = data.aws_iam_policy_document.cw_pipeline_start.json
  role   = aws_iam_role.cw_ecs_pipeline.id
}

resource "aws_cloudwatch_event_rule" "ecr_change" {
  name          = "cw_ecr_change_rule"
  event_pattern = "{\"detail-type\":[\"ECR Image Action\"],\"source\":[\"aws.ecr\"],\"detail\":{\"action-type\":[\"PUSH\"],\"image-tag\":[\"latest\"],\"repository-name\":[\"${aws_ecr_repository.main.name}\"],\"result\":[\"SUCCESS\"]}}"
  role_arn      = aws_iam_role.cw_ecs_pipeline.arn
}

resource "aws_cloudwatch_event_target" "ecr_change" {
  arn      = aws_codepipeline.codepipeline.arn
  rule     = aws_cloudwatch_event_rule.ecr_change.name
  role_arn = aws_iam_role.cw_ecs_pipeline.arn
}
