resource "aws_cloudwatch_log_group" "app" {
  name = "tf-ecs-group/app-revolgy"
}

resource "aws_cloudwatch_log_group" "app_migrations" {
  name = "tf-ecs-group/app-revolgy-migrations"
}