const express = require('express')
const app = express()
const calculateReadTime = require('./src/calculateReadTime')
require('cross-fetch/polyfill');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js')

require('dotenv').config()

const port = process.env.APP_PORT;
const host = process.env.APP_HOST;

const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        port: process.env.DB_PORT,
    }
});

app.use(express.json())

const poolData = {
    UserPoolId: process.env.COGNITO_USER_POOL_ID,
    ClientId: process.env.COGNITO_CLIENT_ID,
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

app.get('/', (req, res) => res.send({'hello': 'World!'}))

app.post('/auth/register', (req, res) => {
    const data = req.body

    if (!data.username || !data.password || !data.email) {
        res.status(400)
        res.send({'message': 'Invalid request'})
        return
    }

    const attributeList = [
        new AmazonCognitoIdentity.CognitoUserAttribute({
            Name: 'email',
            Value: data.email,
        })
    ]

    try {
        userPool.signUp(data.username, data.password, attributeList, null, function (
            err,
            result
        ) {
            if (err) {
                res.status(500)
                res.send({'message': err.message || JSON.stringify(err)});
                return;
            }

            res.status(201)
            const cognitoUser = result.user;
            res.send({'username': cognitoUser.getUsername()});
        });
    } catch (e) {
        res.status(500)
        res.send({'message': e.message});
    }
})

app.post('/auth/login', (req, res) => {
    const data = req.body

    if (!data.username || !data.password) {
        res.status(400)
        res.send({'message': 'Invalid request'})
        return
    }

    const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
        {
            Username: data.username,
            Password: data.password,
        }
    )

    try {
        const cognitoUser = new AmazonCognitoIdentity.CognitoUser({
            Username: data.username,
            Pool: userPool,
        })
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                const accessToken = result.getAccessToken().getJwtToken();

                res.status(200)
                res.send({'jwt_token': accessToken})
            },

            onFailure: function (err) {
                res.status(500)
                res.send({'message': err.message || JSON.stringify(err)});
            },
        });
    } catch (e) {
        res.status(500)
        res.send({'message': e.message});
    }
})

app.get('/posts', async (req, res) => {
    try {
        res.send(await knex('posts'))
    } catch (e) {
        res.status(500)
        res.send({'message': e})
    }
})

app.get('/posts/:id', async (req, res) => {
    const postId = req.params.id

    try {
        const posts = await knex('posts').where('id', postId).limit(1)

        if (posts.length === 0) {
            res.status(404)
            res.send({'message': 'Post does not exist'})
        } else {
            let doc = posts[0]
            doc['read_time'] = calculateReadTime(posts[0].content)

            res.send(doc)
        }
    } catch (e) {
        res.status(500)
        res.send({'message': e})
    }
})

app.post('/posts', async (req, res) => {
    const data = req.body

    if (!data.title || !data.content) {
        res.status(400)
        res.send({'message': 'Invalid request'})
        return
    }

    try {
        res.send(await knex('posts').insert({
            title: data.title,
            content: data.content,
        }))
    } catch (e) {
        res.status(500)
        res.send({'message': e})
    }
})

app.use(function (req, res, next) {
    if (res.headersSent) {
        return next()
    }
    res.status(404)
    res.send({error: 'not_found'})
})

app.listen(port, host, () => console.log(`App listening at http://${host}:${port}`))